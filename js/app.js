//Теория
// Я понимаю прототипное наследование, как способ расширения объекта за счет указания, в качестве его прототипа, другого объекта, который может его расширить своими свойствами и методами.
//Задание
class Employee {
    constructor (name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    };
    get getName() {
        return this.name;
    }
    get getAge() {
        return this.age;
    }
    get getSalary() {
        return this.salary;
    }
    set setName(newName) {
        this.name = newName;
    }
    set setAge(newAge) {
        this.age = newAge;
    }
    set setSalary(newSalary) {
        this.salary = newSalary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }
    get getSalary() {
        return this.salary * 3;
    }
}


const user = new Employee('Alex', 30, 2000);

const user1 = new Programmer('John', 25, 1000, 'EN');

console.log(user);
console.log(user1);
